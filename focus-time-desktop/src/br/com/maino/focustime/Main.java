package br.com.maino.focustime;

import java.awt.Dimension;
import java.awt.Toolkit;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "focus-time";
		cfg.useGL20 = true;
		cfg.fullscreen = true;
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		cfg.width = d.width;
		cfg.height = d.height;		
		
		new LwjglApplication(new FocusTime(), cfg);
	}
}
