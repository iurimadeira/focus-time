package br.com.maino.focustime;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ImageSelector {
	
	private static int stateTimeElapsed = 0;
	private static State actualState = State.FOCUS_TIME;
	
	private static Texture focusTimeTexture;
	private static Sprite focusTimeSprite;
	
	private static Texture intervalTexture;
	private static Sprite intervalSprite;
	
	public static void initializeImages(){
		focusTimeTexture = new Texture(Gdx.files.classpath(XmlHandler.getFocusTimeImage()));		
		intervalTexture = new Texture(Gdx.files.classpath(XmlHandler.getIntervalImage()));
		
		focusTimeTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		intervalTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		TextureRegion focusTimeRegion = new TextureRegion(focusTimeTexture, 0, 0, 1024, 768);
		TextureRegion intervalRegion = new TextureRegion(intervalTexture, 0, 0, 1024, 768);
		
		focusTimeSprite = new Sprite(focusTimeRegion);
		intervalSprite = new Sprite(intervalRegion);
		
		focusTimeSprite.setSize(0.9f, 0.9f * focusTimeSprite.getHeight() / focusTimeSprite.getWidth());
		focusTimeSprite.setOrigin(focusTimeSprite.getWidth()/2, focusTimeSprite.getHeight()/2);
		focusTimeSprite.setPosition(-focusTimeSprite.getWidth()/2, -focusTimeSprite.getHeight()/2);
		
		intervalSprite.setSize(0.9f, 0.9f * intervalSprite.getHeight() / intervalSprite.getWidth());
		intervalSprite.setOrigin(intervalSprite.getWidth()/2, intervalSprite.getHeight()/2);
		intervalSprite.setPosition(-intervalSprite.getWidth()/2, -intervalSprite.getHeight()/2);
	}

	public static Sprite getStateImage(){
		switch(actualState){
			case FOCUS_TIME:				
				return focusTimeSprite;				
			case INTERVAL:				
				return intervalSprite;				
		}
		
		return null;		
	}
	
	private static boolean stateFinished(){
		switch(actualState){
			case FOCUS_TIME:
				if (stateTimeElapsed >= XmlHandler.getDuration(State.FOCUS_TIME)){
					return true;
				}
				return false;
			case INTERVAL:
				if (stateTimeElapsed >= XmlHandler.getDuration(State.INTERVAL)){
					return true;
				}
				return false;
		}		
		
		return true;
	}	
	
	private static void nextState(){
		switch(actualState){
			case FOCUS_TIME:				
				actualState = State.INTERVAL;
				stateTimeElapsed = 0;
				return;
			case INTERVAL:
				actualState = State.FOCUS_TIME;
				stateTimeElapsed = 0;
				return;
		}
	}
	
	public static void disposeTextures(){
		focusTimeTexture.dispose();
		intervalTexture.dispose();
	}
	
	public static void checkState(){
					
			//Se o tempo do estado tiver acabado, muda para pr�ximo
			//Caso n�o terminado, adiciona mais 1 segundo.
			if (stateFinished()){
				nextState();
				System.out.println(actualState);
			} else {
				stateTimeElapsed++;
			}								
			
			//Sleep por 1 segundo
			try {
				Thread.sleep(1000);
			} catch (Exception e){
				e.printStackTrace();
			}		
		
	}
}
