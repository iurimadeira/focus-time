package br.com.maino.focustime;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.XmlReader;

public class XmlHandler {
	
	public static String focusTimeImage;
	public static String intervalImage;
	public static int focusTimeDuration;
	public static int intervalDuration;

	public static String getFocusTimeImage(){
		return focusTimeImage;
	}
	
	public static String getIntervalImage(){
		return intervalImage;
	}
	
	public static int getDuration(State state){
		switch(state){
			case FOCUS_TIME:
				return focusTimeDuration * 60;
			case INTERVAL:
				return intervalDuration * 60;
		}
		
		return 0;		
	}	
	
	public static void parseXML(){
		XmlReader xml = new XmlReader();
		XmlReader.Element xmlElement;		
		
		try {
			xmlElement = xml.parse(Gdx.files.internal("data/focustime.xml"));
			
			focusTimeImage = xmlElement.getChildByName("focus-time-image").get("value");			
			focusTimeDuration = Integer.parseInt(xmlElement.getChildByName("focus-time-duration").get("value"));
			
			intervalImage = xmlElement.getChildByName("interval-image").get("value");
			intervalDuration = Integer.parseInt(xmlElement.getChildByName("interval-duration").get("value"));			
		
		} catch (IOException e) {		
			e.printStackTrace();
		}		
	}
}
